namespace KataPotter
{
    public class Book
    {
        public readonly BookType Type;

        public Book(BookType type)
        {
            Type = type;
        }
    }
}