﻿using System.Collections.Generic;
using NUnit.Framework;

namespace KataPotter
{
    [TestFixture]
    public class PriceCalculatorShould
    {

        static readonly object[] BasketCases =
        {
            new object[] { new List<Book>(), 0, "Empty basket"},
            new object[] { new List<Book>{new Book(BookType.One)}, 8, "One book in the basket"},
            new object[] { new List<Book>{new Book(BookType.One), new Book(BookType.Two)}, 15.20, "Two differnt books in the basket"},
            new object[] { new List<Book>{new Book(BookType.One), new Book(BookType.One)}, 16, "Two same books in the basket"},
            new object[] { new List<Book>{new Book(BookType.One), new Book(BookType.Two), new Book(BookType.Three)}, 21.60, "Three different books in the basket"},
            
        };

        [Test, TestCaseSource("BasketCases")]
        public void return_zero_if_no_books_are_in_the_basket(List<Book> basketItems, double expectedPrice, string description)
        {
            var priceCalculator = new PriceCalculator(basketItems);
            double basketAmount = priceCalculator.Calculate();

            Assert.AreEqual(expectedPrice, basketAmount);
        }
    }
}
