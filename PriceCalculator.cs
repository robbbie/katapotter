﻿using System.Collections.Generic;
using System.Linq;

namespace KataPotter
{
    public class PriceCalculator
    {
        private readonly Dictionary<int, int> _bookQuantityToDiscount = new Dictionary<int, int>
        {
             {0, 0},
             {1, 0},
             {2, 5},
             {3, 10}
        };

        const float SINGLE_BOOK_PRICE = 8.0f;

        private readonly List<Book> _basketItems; 

        public PriceCalculator(List<Book> basketItems)
        {
            _basketItems = basketItems;
        }

        public double Calculate()
        {
            double basePrice = _basketItems.Count * SINGLE_BOOK_PRICE;

            var groupOfDifferentBooks = _basketItems.GroupBy(x=>x.Type).Count();

            return basePrice - (basePrice * _bookQuantityToDiscount[groupOfDifferentBooks]/100);
        }
    }
}
